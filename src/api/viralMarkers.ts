import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const getViralMarkers = async () => {
  try {
    const url = new URL(`${routes.GET_VIRAL_MARKERS}`);

    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });

    if (resp?.status !== 200) {
      throw new Error(`${resp.status} ${resp.statusText}`);
    }

    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

export { getViralMarkers };
