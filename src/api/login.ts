import { ILogin } from '../interfaces/login';
import routes from './routes';

const Login = async (login: ILogin) => {
  const response = await fetch(routes.LOGIN, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ Username: login.user, Password: login.pwd }),
  });

  const paresedResp = await response.json();
  if (response.status !== 200) {
    throw paresedResp.msg;
  }
  return paresedResp;
};

export default Login;
