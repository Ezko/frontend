import { sessionToken } from '../components/utils/constants';
import {
  IChronicPatient,
  IPatientComponent,
  ITransitoryPatient,
} from '../interfaces/patient';
import routes from './routes';

const getPatientById = async (patientId: string) => {
  const resp = await fetch(`${routes.GET_PATIENTS}/${patientId}`, {
    method: 'GET',
    headers: {
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });

  return await resp.json();
};

const createPatient = async (patient: IPatientComponent, type) => {
  let convertedPatient: ITransitoryPatient | IChronicPatient;
  const {
    document,
    email,
    fnrNumber,
    lastName,
    leaveDate,
    name,
    phone,
    secondPhone,
    secureNumber,
    viralMarkers,
  } = patient;
  if (type.IdTipoPaciente === 3) {
    convertedPatient = {
      Tipo: type.NombreTipoPaciente,
      Documento: document,
      Nombre: name,
      Apellido: lastName,
      TelPrincipal: phone,
      TelSecundario: secondPhone,
      Correo: email,
      MarcadoresVirales: viralMarkers,
      NumeroSeguro: secureNumber,
      FechaIngreso: new Date().toISOString().split('T')[0],
      FechaEgreso: new Date(leaveDate).toISOString().split('T')[0],
    };
  } else {
    convertedPatient = {
      Tipo: type.NombreTipoPaciente,
      Documento: document,
      Nombre: name,
      Apellido: lastName,
      TelPrincipal: phone,
      TelSecundario: secondPhone,
      Correo: email,
      MarcadoresVirales: viralMarkers,
      NumeroFondo: fnrNumber,
    };
  }

  const resp = await fetch(`${routes.CREATE_PATIENT}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
    body: JSON.stringify(convertedPatient),
  });

  if (resp?.status >= 400 && resp?.status < 600) {
    throw await resp.json();
  }

  return await resp.json();
};

const getPatientTypes = async () => {
  const resp = await fetch(`${routes.GET_PATIENT_TYPES}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });

  return await resp.json();
};

export { getPatientById, createPatient, getPatientTypes };
