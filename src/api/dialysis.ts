import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const getDialysisByShiftAndRoom = async (
  roomId: number,
  shiftId: number,
  date: Date
) => {
  try {
    const strDate = new Date(date).toISOString().split('T')[0];
    const url = new URL(
      `${routes.GET_DIALYSIS_BY_ROOM_SHIFT_AND_DATE}/${roomId}/${shiftId}/${strDate}`
    );
    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

const getDialysisByPatient = async (
  patientId: string,
  startDate: Date,
  endDate: Date
) => {
  try {
    const strStartDate = new Date(startDate).toISOString().split('T')[0];
    const strEndDate = new Date(endDate).toISOString().split('T')[0];
    const url = new URL(
      `${routes.GET_DIALYSIS_BY_PATIENT}/${patientId}/${strStartDate}/${strEndDate}`
    );
    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

const addDialysis = async (data: any) => {
  const {
    shiftId,
    nurseId,
    patientId,
    workStationId,
    docId,
    graduateId,
    date,
    replicate,
  } = data;
  const dataToSend = {
    Fecha: new Date(date).toISOString().split('T')[0],
    CodTurno: shiftId,
    CedulaEnfermero: nurseId,
    DocPaciente: patientId,
    CodMaquina: workStationId,
    CedulaMedico: docId,
    CedulaLicenciado: graduateId,
    EventoPirogenico: null,
  };
  const url = new URL(`${routes.CRUD_DIALYSIS}/${replicate}`);
  const resp = await fetch(url.toString(), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
    body: JSON.stringify(dataToSend),
  });
  return await resp.json();
};

const updateDialysis = async (data: any) => {
  const {
    dialysisId,
    shiftId,
    nurseId,
    patientId,
    workStationId,
    docId,
    graduateId,
    date,
    replicate,
    pyrogenicEvent,
  } = data;

  const isReplicate = () => (replicate === undefined ? false : replicate);

  const dataToSend = {
    Fecha: new Date(date).toISOString().split('T')[0],
    CodTurno: shiftId,
    CedulaEnfermero: nurseId,
    DocPaciente: patientId,
    CodMaquina: workStationId,
    CedulaMedico: docId,
    CedulaLicenciado: graduateId,
    EventoPirogenico: pyrogenicEvent,
  };
  const url = new URL(`${routes.CRUD_DIALYSIS}/${dialysisId}/${isReplicate()}`);
  const resp = await fetch(url.toString(), {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
    body: JSON.stringify(dataToSend),
  });
  return await resp.json();
};

const deleteDialysis = async (data: any) => {
  const { dialysisId, replicate } = data;
  const url = new URL(`${routes.CRUD_DIALYSIS}/${dialysisId}/${replicate}`);
  const resp = await fetch(url.toString(), {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });
  return await resp.json();
};

export {
  getDialysisByShiftAndRoom,
  getDialysisByPatient,
  addDialysis,
  updateDialysis,
  deleteDialysis,
};
