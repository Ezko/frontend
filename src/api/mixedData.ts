import { getDoctors, getGraduates, getNurses } from './profesional';
import { getworkStationByRoom } from './workStation';

const getAddDialysisData = async (roomId: number) => {
  return await Promise.all([
    (await getNurses()).json(),
    (await getworkStationByRoom(roomId)).json(),
    (await getDoctors()).json(),
    (await getGraduates()).json(),
  ]);
};

const getEditDialysisData = async (roomId: number) => {
  return await Promise.all([
    (await getNurses()).json(),
    (await getworkStationByRoom(roomId)).json(),
  ]);
};

export { getAddDialysisData, getEditDialysisData };
