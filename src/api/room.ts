import { INewRoom } from '../interfaces/room';
import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const newRoom = (room: INewRoom) => {
  return new Promise((resolve, reject) => {
    room.name !== 'boom' ? resolve(200) : reject(new Error('500'));
  });
};

const getRooms = async () => {
  try {
    const resp = await fetch(routes.GET_ROOMS, {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

const getRoomStatus = async (shiftId: number, date: Date) => {
  try {
    const strDate = new Date(date).toISOString().split('T')[0];
    const url = new URL(`${routes.GET_ROOMS_STATUS}/${shiftId}/${strDate}`);
    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

export { newRoom, getRooms, getRoomStatus };
