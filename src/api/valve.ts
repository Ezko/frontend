// import { INewWorkStation } from "../interfaces/crudWorkStation";

import routes from './routes';

const getValvesByRoom = async roomId => {
  const resp = await fetch(`${routes.GET_VALVES_BY_ROOM}/${roomId}`, {
    method: 'GET',
  });
  return await resp.json();
};

export { getValvesByRoom };
