import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const getShiftAndAvailability = async (startDate: Date, endDate: Date) => {
  try {
    const start = startDate.toLocaleDateString().split('/').reverse().join('-'); // convierte la fecha a str local, splitea por / luego inverte el array y lo pasa a string con un - como separador, esto es para lograr el formato año-mes-dia
    const end = endDate.toLocaleDateString().split('/').reverse().join('-');
    const url = new URL(
      `${routes.GET_SHIFTS_AND_AVAILABILITY}/${start}/${end}`
    );

    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });

    if (resp?.status !== 200) {
      throw new Error(`${resp.status} ${resp.statusText}`);
    }

    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

const getShifts = async () => {
  try {
    const url = new URL(`${routes.GET_SHIFTS}`);
    const resp = await fetch(url.toString(), {
      method: 'GET',
      headers: {
        'X-JWT-Token': sessionStorage.getItem(sessionToken),
      },
    });

    if (resp?.status !== 200) {
      throw new Error(`${resp.status} ${resp.statusText}`);
    }

    return await resp.json();
  } catch (error) {
    console.error(error);
  }
};

export { getShiftAndAvailability, getShifts };
