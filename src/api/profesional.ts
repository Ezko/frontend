import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const getNurses = () =>
  fetch(routes.GET_NURSES, {
    method: 'GET',
    headers: {
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });

const getDoctors = () =>
  fetch(routes.GET_DOCTORS, {
    method: 'GET',
    headers: {
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });

const getGraduates = () =>
  fetch(routes.GET_GRADUATES, {
    method: 'GET',
    headers: {
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });

export { getNurses, getDoctors, getGraduates };
