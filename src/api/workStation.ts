import { INewWorkStation } from '../interfaces/workStation';
import { sessionToken } from '../components/utils/constants';
import routes from './routes';

const newWorkStation = async (workStation: INewWorkStation) => {
  const resp = await fetch(routes.NEW_WORK_STATION, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      CodMaquina: workStation.name,
      CodPico: workStation.valve.CodPico,
    }),
  });
  return await resp.json();
};

const getworkStationByRoom = (roomId: number) => {
  const url = new URL(`${routes.GET_WORKSTATION}/${roomId}`);
  return fetch(url.toString(), {
    method: 'GET',
    headers: {
      'X-JWT-Token': sessionStorage.getItem(sessionToken),
    },
  });
};

export { newWorkStation, getworkStationByRoom };
