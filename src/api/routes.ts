const baseUri = 'https://proyectogggbackend.azurewebsites.net';
const prefix = 'api';

export default {
  GET_ROOMS: `${baseUri}/${prefix}/rooms`,
  GET_ROOMS_STATUS: `${baseUri}/${prefix}/rooms/getAvailability`,
  GET_VALVES_BY_ROOM: `${baseUri}/${prefix}/valve`,
  NEW_WORK_STATION: `${baseUri}/${prefix}/machines`,
  GET_WORKSTATION: `${baseUri}/${prefix}/machines`,
  GET_SHIFTS: `${baseUri}/${prefix}/shifts`,
  GET_SHIFTS_AND_AVAILABILITY: `${baseUri}/${prefix}/dialysis/availability`,
  GET_DIALYSIS_BY_ROOM_SHIFT_AND_DATE: `${baseUri}/${prefix}/dialysis/populateShift`,
  GET_DIALYSIS_BY_PATIENT: `${baseUri}/${prefix}/dialysis/patientScheduledDialysis`,
  GET_DOC_BY_ROOM_AND_SHIFT: `${baseUri}/${prefix}/doctors`,
  GET_DOCTORS: `${baseUri}/${prefix}/doctors`,
  GET_NURSES: `${baseUri}/${prefix}/nurses`,
  GET_GRADUATES: `${baseUri}/${prefix}/licensees`,
  GET_PATIENTS: `${baseUri}/${prefix}/patients`,
  CREATE_PATIENT: `${baseUri}/${prefix}/patients`,
  GET_VIRAL_MARKERS: `${baseUri}/${prefix}/viralMarker`,
  LOGIN: `${baseUri}/${prefix}/users/login`,
  CRUD_DIALYSIS: `${baseUri}/${prefix}/dialysis`,
  GET_PATIENT_TYPES: `${baseUri}/${prefix}/patientTypes`,
};
