import { FormControl, Input } from '@chakra-ui/react';
import React from 'react';
import { formCtrl } from '../utils/crudPatientStyles';

const ChronicPatient = ({ patientInfo, handleChange, getIsErrorStyle }) => {
  const { fnrNumber } = patientInfo;

  const handleFnrNumber = event => {
    event.preventDefault();
    const value = event.target.value;
    if (!isNaN(value) && value.length <= 6) {
      handleChange({ ...patientInfo, fnrNumber: value });
    }
  };

  return (
    <FormControl sx={formCtrl}>
      <Input
        placeholder='Numero de fondo'
        variant='fileld'
        value={fnrNumber}
        onChange={handleFnrNumber}
        type='number'
        border={getIsErrorStyle(fnrNumber)}
      />
    </FormControl>
  );
};

export default ChronicPatient;
