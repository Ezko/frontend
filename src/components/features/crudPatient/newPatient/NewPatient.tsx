import React from 'react';
import {
  checkBoxContainer,
  formContainer,
  formCtrl,
  formSubContainer,
  mainNewPatientBox,
  mainTitle,
  selectTyple,
  titleLine,
  typesCollapse,
} from '../utils/crudPatientStyles';
import {
  Box,
  Button,
  Center,
  CheckboxGroup,
  Collapse,
  Divider,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  InputGroup,
  Select,
} from '@chakra-ui/react';
import BasePatient from './BasePatient';
import useNewPatient from '../hooks/useNewPatient';

const NewPatient = () => {
  const {
    isOpen,
    markersList,
    patientInfo,
    patientTypes,
    selectedType,
    generateViralMarkers,
    handleCreate,
    handleSelect,
    typeToShow,
    getIsErrorStyle,
    setPatientInfo,
  } = useNewPatient();

  const { viralMarkers } = patientInfo;

  return (
    <Box sx={mainNewPatientBox}>
      <Heading size='lg' sx={mainTitle}>
        Nuevo Paciente
      </Heading>
      <Divider sx={titleLine} />

      <Center sx={formContainer}>
        <Box sx={formSubContainer}>
          <BasePatient
            patientInfo={patientInfo}
            handleChange={setPatientInfo}
            getIsErrorStyle={getIsErrorStyle}
          />

          <Collapse animateOpacity in={isOpen} style={typesCollapse}>
            {typeToShow()}
          </Collapse>

          <FormControl sx={formCtrl}>
            <InputGroup>
              <Select
                variant='fill'
                placeholder='Tipo de paciente'
                sx={selectTyple}
                onChange={handleSelect}
                border={getIsErrorStyle(selectedType)}
              >
                {patientTypes?.map((type, _i) => (
                  <option key={_i} value={type.IdTipoPaciente}>
                    {type.NombreTipoPaciente}
                  </option>
                ))}
              </Select>

              <FormLabel />

              <CheckboxGroup value={viralMarkers}>
                <HStack spacing={'0px'} sx={checkBoxContainer}>
                  {markersList && generateViralMarkers()}
                </HStack>
              </CheckboxGroup>
            </InputGroup>
          </FormControl>

          <Button isFullWidth onClick={handleCreate} sx={formCtrl}>
            Guardar
          </Button>
        </Box>
      </Center>
    </Box>
  );
};
export default NewPatient;
