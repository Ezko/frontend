import {
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  Tooltip,
} from '@chakra-ui/react';
import React from 'react';
import { formCtrl } from '../utils/crudPatientStyles';

const TransitoryPatient = ({ patientInfo, handleChange, getIsErrorStyle }) => {
  const { secureNumber, leaveDate } = patientInfo;

  const handleSecureNumber = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, secureNumber: event.target.value });
  };

  const handleLeaveDate = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, leaveDate: event.target.value });
  };

  return (
    <FormControl sx={formCtrl}>
      <InputGroup>
        <Input
          placeholder='Numero de seguro'
          variant='fileld'
          value={secureNumber}
          onChange={handleSecureNumber}
          type='number'
          border={getIsErrorStyle(secureNumber)}
        />
        <FormLabel />
        <Tooltip hasArrow placement='right' label='Fecha de egreso'>
          <Input
            placeholder='Fecha de egreso'
            type='date'
            variant='fileld'
            value={leaveDate}
            onChange={handleLeaveDate}
            sx={{ color: 'gray.400' }}
            border={getIsErrorStyle(leaveDate)}
          />
        </Tooltip>
      </InputGroup>
    </FormControl>
  );
};

export default TransitoryPatient;
