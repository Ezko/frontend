import { FormControl, FormLabel, Input, InputGroup } from '@chakra-ui/react';
import React from 'react';
import { formCtrl } from '../utils/crudPatientStyles';

const BasePatient = ({ patientInfo, handleChange, getIsErrorStyle }) => {
  const { name, lastName, phone, secondPhone, email, document } = patientInfo;

  const handleDocument = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, document: event.target.value });
  };
  const handleName = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, name: event.target.value });
  };
  const handleLastName = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, lastName: event.target.value });
  };
  const handlePhone = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, phone: event.target.value });
  };
  const handleSecondPhone = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, secondPhone: event.target.value });
  };
  const handleEmail = event => {
    event.preventDefault();
    handleChange({ ...patientInfo, email: event.target.value });
  };

  return (
    <>
      <FormControl sx={formCtrl}>
        <Input
          border={getIsErrorStyle(document)}
          onChange={handleDocument}
          placeholder='Documento'
          variant='fileld'
          value={document}
          type='number'
        />
      </FormControl>

      <FormControl sx={formCtrl}>
        <InputGroup>
          <Input
            border={getIsErrorStyle(name)}
            onChange={handleName}
            placeholder='Nombre'
            variant='fileld'
            value={name}
          />
          <FormLabel />
          <Input
            border={getIsErrorStyle(lastName)}
            onChange={handleLastName}
            placeholder='Apellido'
            variant='fileld'
            value={lastName}
          />
        </InputGroup>
      </FormControl>

      <FormControl sx={formCtrl}>
        <InputGroup>
          <Input
            border={getIsErrorStyle(phone)}
            onChange={handlePhone}
            placeholder='Tel. principal'
            variant='fileld'
            value={phone}
            type='number'
          />
          <FormLabel />
          <Input
            onChange={handleSecondPhone}
            placeholder='Tel. alternativo'
            variant='fileld'
            value={secondPhone}
            type='number'
          />
        </InputGroup>
      </FormControl>

      <FormControl sx={formCtrl}>
        <Input
          border={getIsErrorStyle(email)}
          onChange={handleEmail}
          placeholder='Correo'
          variant='fileld'
          value={email}
        />
      </FormControl>
    </>
  );
};

export default BasePatient;
