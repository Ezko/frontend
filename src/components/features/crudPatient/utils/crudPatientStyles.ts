export const mainNewPatientBox = {
  width: { md: '35%', sm: '70%' },
  height: '460px',
  marginTop: { md: '0', sm: '350px' },
  marginBot: '50px',
  bg: 'gray.400',
  color: 'black',
  borderRadius: 'lg',
  p: '5',
  boxShadow: '10px 10px 10px black',
};

export const mainTitle = {
  textAlign: 'center',
};

export const titleLine = {
  marginTop: '8px',
};

export const formCtrl = {
  marginTop: '10px',
};

export const selectTyple = {
  color: 'gray.400',
};

export const checkBoxContainer = {
  justifyContent: 'center',
  flexWrap: 'wrap',
};

export const checkBox = {
  marginRight: '12px!important',
};

export const formContainer = {
  height: '90%',
  overflow: 'auto',
};

export const formSubContainer = {
  padding: '8px 0',
  width: '100%',
};

export const typesCollapse = {
  width: '100%',
};
