import React, { useEffect, useState } from 'react';
import { checkBox } from '../utils/crudPatientStyles';
import { Checkbox, useDisclosure, useToast } from '@chakra-ui/react';
import { getViralMarkers } from '../../../../api/viralMarkers';
import { createPatient, getPatientTypes } from '../../../../api/patient';
import { IPatientComponent } from '../../../../interfaces/patient';
import TransitoryPatient from '../newPatient/TransitoryPatient';
import ChronicPatient from '../newPatient/ChronicPatient';

const patientClearData: IPatientComponent = {
  name: '',
  lastName: '',
  phone: '',
  secondPhone: '',
  email: '',
  document: '',
  viralMarkers: [],
  secureNumber: '',
  leaveDate: '',
  fnrNumber: '',
};

const useNewPatient = () => {
  const { isOpen, onToggle } = useDisclosure();
  const toast = useToast();
  const [patientTypes, setPatientTypes] = useState([]);
  const [selectedType, setSelectedType] = useState(undefined);
  const [markersList, setMarkersList] = useState([]);
  const [isError, setIsError] = useState(false);
  const [patientInfo, setPatientInfo] = useState<IPatientComponent>({
    ...patientClearData,
  });

  const {
    name,
    lastName,
    phone,
    email,
    document,
    viralMarkers,
    secureNumber,
    leaveDate,
    fnrNumber,
  } = patientInfo;

  useEffect(() => {
    const getTypes = async () => {
      const loadPatientTypes = async () => {
        try {
          const { types } = await getPatientTypes();
          if (types.length <= 0) {
            throw 'Algo salio mal al cargar los tipos';
          }
          const aux = types.filter(type => type.NombreTipoPaciente !== 'Agudo'); // Por ahora los agudos no seran incluidos, por ese motivo los estamos filtrando
          setPatientTypes(aux);
        } catch (err) {
          console.error(err);
          return toast({
            title: 'Fallo al cargar tipos de paciente',
            status: 'error',
            position: 'top',
            duration: 9000,
            isClosable: true,
          });
        }
      };
      loadPatientTypes();
    };
    getTypes();
  }, []);

  useEffect(() => {
    const loadViralMarkers = async () => {
      try {
        const { markers } = await getViralMarkers();
        if (markers.length <= 0) {
          throw 'Algo salio mal al cargar los Marcadores';
        }
        setMarkersList(markers);
      } catch (err) {
        console.error(err);
        return toast({
          title: 'Fallo al cargar los marcadores virales',
          status: 'error',
          position: 'top',
          duration: 9000,
          isClosable: true,
        });
      }
    };
    loadViralMarkers();
  }, []);

  const handleSelect = async event => {
    event.preventDefault();
    if (isOpen) await onToggle();
    else onToggle();
    setSelectedType(event.target.value);
  };

  const typeToShow = () => {
    switch (selectedType) {
      case '3':
        setTimeout(() => {
          if (!isOpen) onToggle();
        }, 100);
        return (
          <TransitoryPatient
            patientInfo={patientInfo}
            handleChange={setPatientInfo}
            getIsErrorStyle={getIsErrorStyle}
          />
        );
      case '2':
        setTimeout(() => {
          if (!isOpen) onToggle();
        }, 100);
        return (
          <ChronicPatient
            patientInfo={patientInfo}
            handleChange={setPatientInfo}
            getIsErrorStyle={getIsErrorStyle}
          />
        );
    }
  };

  const handleViralMarkers = event => {
    const value = parseInt(event.target.value);
    let updatedMarkers = [...viralMarkers];

    if (viralMarkers.includes(value)) {
      updatedMarkers = viralMarkers.filter(marker => marker !== value);
    } else {
      updatedMarkers.push(value);
    }

    setPatientInfo({ ...patientInfo, viralMarkers: updatedMarkers });
  };

  const generateViralMarkers = () =>
    markersList.map((marker, _i) => (
      <Checkbox
        key={_i}
        value={marker.IdMarcadorViral}
        onChange={handleViralMarkers}
        sx={checkBox}
      >
        {marker.NombreMarcadorViral}
      </Checkbox>
    ));

  const handleCreate = async () => {
    const toastInfo = { message: '', level: 'info' };
    if (!userInfoIsValid()) {
      setIsError(true);
      toastInfo.message = 'Debe completar los campos indicados';
      toastInfo.level = 'error';
    } else {
      setIsError(false);

      try {
        await createPatient(
          patientInfo,
          patientTypes.find(
            type => type.IdTipoPaciente.toString() === selectedType
          )
        );
        setPatientInfo({ ...patientClearData });
        toastInfo.message = 'Paciente dado de alta';
        toastInfo.level = 'success';
      } catch (error) {
        console.error(error);
        toastInfo.message = error.msg;
        toastInfo.level = 'error';
      }
    }

    return toast({
      title: `${toastInfo.message}`,
      // @ts-ignore
      status: `${toastInfo.level}`,
      position: 'top',
      duration: 9000,
      isClosable: true,
    });
  };

  const userInfoIsValid = () => {
    if (!selectedType) return false;

    const emailRegex =
      /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,3}(?:\.[a-z]{2})?)$/i;

    if (!emailRegex.exec(email)) {
      setPatientInfo({ ...patientInfo, email: '' });
      return false;
    }

    if (new Date(leaveDate) < new Date()) {
      setPatientInfo({ ...patientInfo, leaveDate: '' });
      return false;
    }

    const basePatientValidation = name && lastName && document && phone;
    const transitoryPatientValidation = secureNumber;
    const chronicPatientValidation = !!fnrNumber;

    if (selectedType === '3') {
      return basePatientValidation && transitoryPatientValidation;
    } else {
      return basePatientValidation && chronicPatientValidation;
    }
  };

  const getIsErrorStyle = value => {
    return !value && isError ? '1.5px solid #F56565' : null;
  };

  return {
    isOpen,
    patientTypes,
    selectedType,
    markersList,
    patientInfo,
    setPatientInfo,
    handleSelect,
    handleCreate,
    typeToShow,
    generateViralMarkers,
    getIsErrorStyle,
  };
};

export default useNewPatient;
