import React, { useEffect, useState } from 'react';
import login from '../../../api/login';
import {
  Box,
  Input,
  Button,
  VStack,
  FormControl,
  FormLabel,
  useToast,
  Heading,
} from '@chakra-ui/react';
import { useNavigate } from 'react-router';
import { COMPONENT_ROUTES, sessionToken } from '../../utils/constants';

const Login = () => {
  const [user, setUser] = useState('');
  const [pwd, setPwd] = useState('');
  const toast = useToast();
  const navigate = useNavigate();

  const handleUser = e => setUser(e.target.value);

  const handlePwd = e => setPwd(e.target.value);

  const handleLogin = async () => {
    try {
      const result = await login({ user, pwd });
      sessionStorage.setItem(sessionToken, result.token);
      navigate(COMPONENT_ROUTES.coordinationPerWeek);
    } catch (error) {
      if (typeof error === 'string') {
        return toast({
          title: `${error}`,
          status: 'error',
          position: 'top',
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  return (
    <Box bg='gray.400' color='black' borderRadius='lg' p='32px 0' w='400px'>
      <VStack align='center' w='100%' spacing={3}>
        <Heading>Iniciar sesión</Heading>
        <VStack w='70%' spacing={4}>
          <FormControl>
            <FormLabel>Usuario</FormLabel>
            <Input
              value={user}
              onChange={handleUser}
              type='text'
              variant='filled'
              bg='white'
              _focus={{ bg: 'gray.200' }}
            />
          </FormControl>

          <FormControl>
            <FormLabel>Contraseña</FormLabel>
            <Input
              value={pwd}
              onChange={handlePwd}
              type='password'
              variant='filled'
              bg='white'
              _focus={{ bg: 'gray.200' }}
            />
          </FormControl>

          <Button isFullWidth onClick={handleLogin}>
            Iniciar
          </Button>
        </VStack>
      </VStack>
    </Box>
  );
};
export default Login;
