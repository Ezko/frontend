import React, { FC, ReactElement } from 'react';
import { Navigate } from 'react-router';
import { sessionToken } from '../../utils/constants';

interface Props {
  children: ReactElement;
}

const AuthValidator: FC<Props> = ({ children }) => {
  return sessionStorage.getItem(sessionToken) ? children : <Navigate to='/' />;
};

export default AuthValidator;
