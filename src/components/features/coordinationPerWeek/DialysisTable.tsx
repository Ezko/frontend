import React, { useContext, useState } from 'react';
import {
  AddIcon,
  DeleteIcon,
  EditIcon,
  StarIcon,
  WarningIcon,
} from '@chakra-ui/icons';
import { DialysisContext } from './utils/DialysisContext';
import { AddDialysisBody, AddDialysisHeader } from './AddDialysis';
import { EditDialysisBody, EditDialysisHeader } from './EditDialysis';
import { DeleteDialysisBody, DeleteDialysisHeader } from './DeleteDialysis';
import {
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Table,
  TableCaption,
  Tbody,
  Td,
  Th,
  Thead,
  Tooltip,
  Tr,
  useDisclosure,
} from '@chakra-ui/react';
import { MODAL_TARGET } from '../../utils/constants';
import { PyrogenicEventBody, PyrogenicEventHeader } from './PyrogenicEvent';

const DialysisTable = ({ availability, updateDialysis }) => {
  const [targetModal, setTargetModal] = useState(MODAL_TARGET.DELETE);
  const [dialysisIdToActions, setDialysisIdToActions] = useState();
  const [dialysis, { shiftId: selectedShift }]: any =
    useContext(DialysisContext);
  const { isOpen, onOpen, onClose: onCancel } = useDisclosure();

  const showAdd = availability !== 'FULL';

  const handleModalClose = async () => {
    await updateDialysis();
    onCancel();
  };

  const getHeader = target => {
    switch (target) {
      case MODAL_TARGET.DELETE:
        return <DeleteDialysisHeader />;
      case MODAL_TARGET.UPDATE:
        return <EditDialysisHeader />;
      case MODAL_TARGET.ADD:
        return <AddDialysisHeader />;
      case MODAL_TARGET.PYROGENIC:
        return <PyrogenicEventHeader />;
    }
  };

  const getBody = target => {
    switch (target) {
      case MODAL_TARGET.DELETE:
        return (
          <DeleteDialysisBody
            dialysisId={dialysisIdToActions}
            onCancel={onCancel}
            onClose={handleModalClose}
          />
        );
      case MODAL_TARGET.UPDATE:
        return (
          <EditDialysisBody
            dialysisId={dialysisIdToActions}
            onCancel={onCancel}
            onClose={handleModalClose}
          />
        );
      case MODAL_TARGET.ADD:
        return (
          <AddDialysisBody onCancel={onCancel} onClose={handleModalClose} />
        );
      case MODAL_TARGET.PYROGENIC:
        return (
          <PyrogenicEventBody
            dialysisId={dialysisIdToActions}
            onCancel={onCancel}
            onClose={handleModalClose}
          />
        );
    }
  };

  const modal = (
    <Modal isCentered onClose={onCancel} isOpen={isOpen}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader sx={{ borderBottom: '1px solid #E2E8F0' }}>
          {getHeader(targetModal)}
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody>{getBody(targetModal)}</ModalBody>
      </ModalContent>
    </Modal>
  );

  const getTableBody = () =>
    dialysis.map((one, index) => {
      if (one.shiftId === selectedShift) {
        return (
          <Tr key={index}>
            <Td>
              {one.patientFirstName} {one.patientLastName}
            </Td>
            <Td>{one.machineId}</Td>
            <Td>
              {one.nurseFirstName} {one.nurseLastName}
            </Td>
            <Td>
              <HStack spacing={4} sx={{ justifyContent: 'center' }}>
                <Tooltip label='Evento pirogénico'>
                  <StarIcon
                    onClick={() => handlePyrogenicEvent(one.dialysisId)}
                    sx={{ cursor: 'pointer' }}
                    color={one.pyrogenicEvent && 'orange.600'}
                  />
                </Tooltip>

                <EditIcon
                  onClick={() => handleUpdate(one.dialysisId)}
                  sx={{ cursor: 'pointer' }}
                />
                <DeleteIcon
                  onClick={() => handleDelete(one.dialysisId)}
                  sx={{ cursor: 'pointer' }}
                />
              </HStack>
            </Td>
          </Tr>
        );
      }
      return null;
    });

  const handleDelete = id => {
    setDialysisIdToActions(id);
    setTargetModal(MODAL_TARGET.DELETE);
    onOpen();
  };

  const handleUpdate = id => {
    setDialysisIdToActions(id);
    setTargetModal(MODAL_TARGET.UPDATE);
    onOpen();
  };

  const handlePyrogenicEvent = id => {
    setDialysisIdToActions(id);
    setTargetModal(MODAL_TARGET.PYROGENIC);
    onOpen();
  };

  const handleAdd = () => {
    setTargetModal(MODAL_TARGET.ADD);
    onOpen();
  };

  return (
    <>
      <Table width='100%' size='sm'>
        {showAdd && (
          <TableCaption>
            <AddIcon
              onClick={handleAdd}
              sx={{
                width: '100px',
                cursor: 'pointer',
              }}
            />
          </TableCaption>
        )}
        <Thead>
          <Tr>
            <Th>Paciente</Th>
            <Th>Maquina</Th>
            <Th>Enfermero</Th>
            <Th textAlign='center'>Acciones</Th>
          </Tr>
        </Thead>
        <Tbody>{getTableBody()}</Tbody>
      </Table>
      {modal}
    </>
  );
};

export default DialysisTable;
