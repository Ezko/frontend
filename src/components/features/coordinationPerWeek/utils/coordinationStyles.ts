export const mainCoordinationBox = {
  width: { md: '85%', sm: '95%' },
  height: { md: '80%', sm: '90%' },
  bg: 'gray.400',
  color: 'black',
  borderRadius: 'lg',
  p: '5',
  boxShadow: '10px 10px 10px black',
};

export const detailWrapperBox = {
  width: { md: '55%', sm: '100%' },
  border: '1px solid black',
  borderRadius: 'lg',
  p: '5',
};

export const weekWrapperBox = {
  overflow: 'auto',
  width: '100%',
  border: '1px solid black',
  borderRadius: 'lg',
  p: '5',
  height: '50%',
};

export const patientWrapperBox = {
  overflow: 'auto',
  width: '100%',
  border: '1px solid black',
  borderRadius: 'lg',
  p: '5',
  height: '50%',
};

export const weekAndPatientWrapper = {
  width: { md: '45%', sm: '100%' },
};

export const shiftBox = {
  textAlign: 'center',
  width: '50px',
  border: '1px solid black',
  borderRadius: 'lg',
  cursor: 'pointer',
  marginTop: '8px',
};

export const shiftWaiting = { mt: 2 };
