export const salasHeading = {
  textAlign: 'center',
  mb: '16px',
};

export const boxRoom = {
  p: '1',
  border: '1px solid black',
  borderRadius: 'lg',
  textAlign: 'center',
  cursor: 'pointer',
  width: '100px',
  marginInlineStart: 0,
};

export const skeletonStyle = {
  m: '8px 0 8px 0',
  height: '25px',
  borderRadius: 'lg',
};

export const dividerStyle = {
  m: '8px 10px',
  width: 'calc(100% - 20px)',
};

export const docAndGraduate = {
  mt: '10px',
  fontSize: '14px',
  width: '100%',
  pl: '16px',
};
