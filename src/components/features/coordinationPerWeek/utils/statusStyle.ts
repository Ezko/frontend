import { STATUS } from '../../../utils/constants';

export const statusStyle = (status, selected) => {
  let color = '';
  let tone = '.400';
  switch (status) {
    case STATUS.EMPTY:
      color = 'green';
      break;
    case STATUS.PARTIAL:
      color = 'yellow';
      break;
    case STATUS.FULL:
      color = 'red';
      break;
  }
  if (selected) tone = '.100';

  return !color ? color : color + tone;
};
