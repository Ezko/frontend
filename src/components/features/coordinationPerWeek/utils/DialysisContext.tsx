import { createContext } from 'react';

const DialysisContext = createContext([]);

export { DialysisContext };
