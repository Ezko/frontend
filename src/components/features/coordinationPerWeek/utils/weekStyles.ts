export const headerBox = {
  position: 'relative',
  mb: '5',
};

export const headerMainStack = {
  justifyContent: 'center',
};

export const headerTitle = {
  textAlign: 'center',
};

export const headerFocusWeek = {
  position: 'absolute',
  top: '12px',
  right: 0,
};
