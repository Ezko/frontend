export const inputDoc = {
  width: '100px',
};

export const inputNamePatient = {
  bgColor: 'gray.200',
  borderRadius: 'lg',
  fontWeight: '400',
  color: 'black',
};

export const formControl = {
  mt: '10px',
};

export const heading = {
  fontSize: '2xl',
  textAlign: 'center',
};

export const switchFormControl = {
  display: 'flex',

  justifyContent: 'center',
  mt: '8px',
};

export const switchDeleteFormControl = {
  display: 'flex',
  alignItems: 'center',
  mt: '8px',
};
