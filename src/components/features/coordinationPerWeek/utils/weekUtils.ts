const sevenDaysInMs = 7 * 86400000;

const days = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];

const months = [
  'Ene',
  'Feb',
  'Mar',
  'Abr',
  'May',
  'Jun',
  'Jul',
  'Ago',
  'Sep',
  'Oct',
  'Nov',
  'Dic',
];

const calculateWeek = () => {
  // [d, l, m, m, j, v, s]
  // [0, 1, 2, 3, 4, 5, 6]
  const today = new Date();
  const dayOfWeek = today.getDay();
  if (dayOfWeek > 0) {
    today.setDate(today.getDate() - dayOfWeek);
  }
  const week = [];
  for (let i = 0; i <= 6; i++) {
    today.setDate(today.getDate() + 1);
    week.push(new Date(today.getTime()));
  }

  return week;
};

const reduceShifts = shiftsArray =>
  shiftsArray.reduce((prev, curr) => {
    const currentDate = new Date(curr.date);
    const target = prev.find(element => element[`${currentDate.getUTCDate()}`]);
    if (target) {
      target[`${currentDate.getUTCDate()}`].push(curr);
    } else {
      const key = currentDate.getUTCDate();
      const obj = {};
      obj[`${key}`] = [curr];
      prev.push(obj);
    }
    return prev;
  }, []);

export { calculateWeek, days, months, sevenDaysInMs, reduceShifts };
