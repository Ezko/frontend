import React, { useEffect, useState } from 'react';
import { DeleteIcon, StarIcon } from '@chakra-ui/icons';
import { DeleteDialysisBody, DeleteDialysisHeader } from './DeleteDialysis';
import {
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Stack,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tooltip,
  Tr,
  useDisclosure,
} from '@chakra-ui/react';
import { days } from './utils/weekUtils';
import { getShifts } from '../../../api/shifts';
import { MODAL_TARGET } from '../../utils/constants';
import { PyrogenicEventBody, PyrogenicEventHeader } from './PyrogenicEvent';

const PatientTable = ({ dialysis, updateDialysis }) => {
  const [targetModal, setTargetModal] = useState(MODAL_TARGET.DELETE);
  const [dialysisId, setDialysisId] = useState();
  const [shiftsData, setShiftsData] = useState<[]>();
  const { isOpen, onOpen, onClose: onCancel } = useDisclosure();

  useEffect(() => {
    const loadShiftsData = async () => {
      const resp = await getShifts();
      setShiftsData(resp?.shifts);
    };
    loadShiftsData();
  }, []);

  const handleModalClose = async () => {
    await updateDialysis();
    onCancel();
  };

  const getTargetDialysis = id => dialysis.find(d => d.dialysisId === id);

  const getHeader = target => {
    switch (target) {
      case MODAL_TARGET.DELETE:
        return <DeleteDialysisHeader />;
      case MODAL_TARGET.PYROGENIC:
        return <PyrogenicEventHeader />;
    }
  };

  const getBody = target => {
    switch (target) {
      case MODAL_TARGET.DELETE:
        return (
          <DeleteDialysisBody
            dialysisId={dialysisId}
            onCancel={onCancel}
            onClose={handleModalClose}
          />
        );
      case MODAL_TARGET.PYROGENIC:
        return (
          <PyrogenicEventBody
            dialysisId={dialysisId}
            onCancel={onCancel}
            onClose={handleModalClose}
            targetDialysis={getTargetDialysis(dialysisId)}
          />
        );
    }
  };

  const modal = (
    <Modal isCentered onClose={onCancel} isOpen={isOpen}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader sx={{ borderBottom: '1px solid #E2E8F0' }}>
          {getHeader(targetModal)}
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody>{getBody(targetModal)}</ModalBody>
      </ModalContent>
    </Modal>
  );

  const handleDelete = id => {
    setDialysisId(id);
    setTargetModal(MODAL_TARGET.DELETE);
    onOpen();
  };

  const handlePyrogenicEvent = id => {
    setDialysisId(id);
    setTargetModal(MODAL_TARGET.PYROGENIC);
    onOpen();
  };

  const getDate = dateStr => {
    const date = new Date(dateStr);
    const day = days[date.getDay()];
    console.log('str', dateStr);
    console.log('dias', JSON.stringify(days));
    console.log('day getted', date.getDay());
    return (
      <>
        {day}
        <br />
        {date.toLocaleDateString()}
      </>
    );
  };

  const shiftName = id => {
    const shiftFinded = shiftsData?.find((shift: any) => shift.CodTurno === id);
    // @ts-ignore
    return shiftFinded ? shiftFinded.Nombre[0] : id;
  };

  const generateTbody = () =>
    dialysis?.map((one: any, index) => (
      <Tr key={index}>
        <Tooltip label='Info de paciente proximamente'>
          <Td p='4px 12px' verticalAlign='middle'>
            {one?.patientFirstName} {one?.patientLastName}
          </Td>
        </Tooltip>
        <Td p='4px 12px' textAlign='center' verticalAlign='middle'>
          {one?.machineId}
        </Td>
        <Td p='4px 12px' verticalAlign='middle'>
          {one?.nurseFirstName} {one?.nurseLastName}
        </Td>
        <Td p='4px 12px' textAlign='center' verticalAlign='middle'>
          {shiftName(one?.shiftId)}
        </Td>
        <Td p='4px 12px' textAlign='center' verticalAlign='middle'>
          {getDate(one?.date)}
        </Td>
        <Td p='4px 12px' textAlign='center' verticalAlign='middle'>
          <HStack spacing={4} sx={{ justifyContent: 'center' }}>
            <Tooltip label='Evento pirogénico'>
              <StarIcon
                onClick={() => handlePyrogenicEvent(one.dialysisId)}
                sx={{ cursor: 'pointer' }}
                color={one.pyrogenicEvent && 'orange.600'}
              />
            </Tooltip>
            <DeleteIcon
              onClick={() => handleDelete(one?.dialysisId)}
              sx={{ cursor: 'pointer' }}
            />
          </HStack>
        </Td>
      </Tr>
    ));

  return (
    <Stack w='100%'>
      <Table width='100%' size='sm'>
        <Thead>
          <Tr>
            <Th p='4px 12px'>Paciente</Th>
            <Th p='4px 12px' textAlign='center'>
              Maquina
            </Th>
            <Th p='4px 12px'>Enfermero</Th>
            <Th p='4px 12px' textAlign='center'>
              Turno
            </Th>
            <Th p='4px 12px' textAlign='center'>
              Fecha
            </Th>
            <Th p='4px 12px' textAlign='center'>
              Acciones
            </Th>
          </Tr>
        </Thead>
        <Tbody>{generateTbody()}</Tbody>
      </Table>
      {modal}
    </Stack>
  );
};

export default PatientTable;
