import React, { useEffect, useState } from 'react';
import {
  Center,
  FormControl,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  Spinner,
} from '@chakra-ui/react';
import { heading } from './utils/patientDetailStyles';
import { SearchIcon } from '@chakra-ui/icons';
import PatientTable from './PatientTable';
import { getDialysisByPatient } from '../../../api/dialysis';

const PatientDetail = ({ refresh, week, shouldRefresh }) => {
  const [dialysis, setDialysis] = useState();
  const [patientId, setPatientId] = useState('');

  useEffect(() => {
    if (patientId) {
      getDialysis();
    }
  }, [patientId, week, shouldRefresh]);

  const getDialysis = async () => {
    const responseDialysis = await getDialysisByPatient(
      patientId,
      week[0],
      week[6]
    );
    if (responseDialysis?.length === 0) {
      setDialysis(undefined);
    } else {
      responseDialysis
        ? setDialysis(responseDialysis)
        : console.error('Algo salio mal al cargar los Dialysis');
    }
  };

  const updateData = () => {
    getDialysis();
    refresh();
  };

  return (
    <>
      <Heading size='md' sx={heading}>
        Coordinaciones de paciente
      </Heading>
      <FormControl mb='16px'>
        <InputGroup size='xs'>
          <InputLeftElement>
            <SearchIcon boxSize={3} />
          </InputLeftElement>

          <Input
            variant='flushed'
            placeholder='Documento'
            value={patientId}
            onChange={e => setPatientId(e.target.value)}
          />
        </InputGroup>
      </FormControl>
      {!dialysis && patientId !== '' ? (
        <Center height='70%'>
          <Spinner height='80px' width='80px' speed='0.8s' />
        </Center>
      ) : (
        <PatientTable dialysis={dialysis} updateDialysis={updateData} />
      )}
    </>
  );
};

export default PatientDetail;
