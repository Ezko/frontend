import React, { useContext, useEffect, useState } from 'react';
import { DialysisContext } from './utils/DialysisContext';
import {
  Button,
  ButtonGroup,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  Select,
  Switch,
  useToast,
} from '@chakra-ui/react';
import {
  formControl,
  heading,
  inputNamePatient,
  switchFormControl,
} from './utils/crudDialysisStyles';
import { getEditDialysisData } from '../../../api/mixedData';
import { updateDialysis } from '../../../api/dialysis';

const EditDialysisHeader = () => {
  return <Heading sx={heading}>Modificar cita</Heading>;
};

const EditDialysisBody = ({ dialysisId, onCancel, onClose }) => {
  const [allDialysis, { selectedRoomId }]: any = useContext(DialysisContext);
  const dialysis = allDialysis.find(
    element => element.dialysisId === dialysisId
  );

  const [loading, setLoading] = useState(false);
  const [replicateData, setReplicateData] = useState(false); // Utilizado para indicar si la acciona a tomar sera puntual o debe aplicar a coordinaciones futuras
  const [workStation, setWorkStation] = useState<[]>();
  const [selectedWorkStation, setSelectedWorkStation] = useState(
    dialysis.machineId
  );
  const [nurse, setNurse] = useState<[]>();
  const [selectedNurse, setSelectedNurse] = useState(dialysis.nurseId);
  const [patient, setPatient] = useState({
    patientFirstName: dialysis.patientFirstName,
    patientLastName: dialysis.patientLastName,
    patientId: dialysis.patientId,
  });

  const toast = useToast();

  useEffect(() => {
    const loadNurseAndworkStations = async () => {
      const resps = await getEditDialysisData(selectedRoomId);
      setNurse(resps[0].nurses);
      setWorkStation(resps[1]);
    };
    loadNurseAndworkStations();
  }, [dialysis]);

  const generateNurseOptions = () =>
    nurse?.map((nurse: any, index) => (
      <option key={index} value={nurse.Cedula}>
        {nurse.Nombre} {nurse.Apellido}
      </option>
    ));

  const generateWorkStationOptions = () =>
    workStation?.map((wk: any, index) => (
      <option key={index} value={wk.CodMaquina}>
        {wk.CodMaquina}
      </option>
    ));

  const validateData = () =>
    (selectedNurse && dialysis.nurseId !== selectedNurse) ||
    (selectedWorkStation && dialysis.machineId !== selectedWorkStation);

  const handleUpdate = async () => {
    if (validateData()) {
      const data = {
        dialysisId: dialysis.dialysisId,
        shiftId: dialysis.shiftId,
        nurseId: selectedNurse,
        patientId: dialysis.patientId,
        workStationId: selectedWorkStation,
        docId: dialysis.doctorId,
        graduateId: dialysis.licenseeId,
        date: dialysis.date,
        replicate: replicateData,
      };
      try {
        setLoading(!loading);
        await updateDialysis(data);
        onClose();
      } catch (error) {
        console.error(error);
      }
    } else {
      return toast({
        title: 'Debe modficiar los campos.',
        status: 'warning',
        position: 'top',
        duration: 9000,
        isClosable: true,
      });
    }
  };

  return (
    <form>
      <FormControl>
        <Input
          disabled
          variant='fileld'
          value={`${patient?.patientFirstName} ${patient?.patientLastName}`}
          sx={inputNamePatient}
        />
      </FormControl>

      <FormControl sx={formControl}>
        <InputGroup>
          <Select
            placeholder='Enfermero'
            value={selectedNurse}
            onChange={e => setSelectedNurse(e.target.value)}
          >
            {!!nurse && generateNurseOptions()}
          </Select>
          <FormLabel />
          <Select
            placeholder='Maquina'
            value={selectedWorkStation}
            onChange={e => setSelectedWorkStation(e.target.value)}
          >
            {!!workStation && generateWorkStationOptions()}
          </Select>
        </InputGroup>
      </FormControl>

      <FormControl sx={switchFormControl}>
        <FormLabel htmlFor='replicate' sx={{ mb: 0 }}>
          Replicar acción
        </FormLabel>
        <Switch
          id='replicate'
          size='sm'
          isChecked={replicateData}
          onChange={() => setReplicateData(!replicateData)}
        />
      </FormControl>

      <ButtonGroup isAttached w='100%' m='18px 0 10px 0'>
        <Button isFullWidth bg='gray' onClick={onCancel}>
          Cancelar
        </Button>
        <Button
          isFullWidth
          isLoading={loading}
          loadingText='Modificando'
          onClick={handleUpdate}
        >
          Guardar
        </Button>
      </ButtonGroup>
    </form>
  );
};

export { EditDialysisHeader, EditDialysisBody };
