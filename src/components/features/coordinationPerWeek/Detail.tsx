import React, { FC, useEffect, useState } from 'react';
import { getDialysisByShiftAndRoom } from '../../../api/dialysis';
import { getRooms, getRoomStatus } from '../../../api/room';
import { IRoom } from '../../../interfaces/room';
import DialysisTable from './DialysisTable';
import { DialysisContext } from './utils/DialysisContext';
import {
  Divider,
  Box,
  Skeleton,
  Text,
  Spinner,
  Center,
  VStack,
  HStack,
  Heading,
} from '@chakra-ui/react';
import {
  boxRoom,
  dividerStyle,
  docAndGraduate,
  salasHeading,
  skeletonStyle,
} from './utils/detailStyles';
import { statusStyle } from './utils/statusStyle';

interface Props {
  shiftId: number;
  date: Date;
  refresh: () => void;
  shouldRefresh: boolean;
}

const Detail: FC<Props> = ({ shiftId, date, refresh, shouldRefresh }) => {
  const [rooms, setRooms] = useState<IRoom[]>();
  const [roomStatus, setRoomStatus] = useState<[]>();
  const [doctor, setDoctor] = useState('');
  const [graduate, setGraduate] = useState('');
  const [dialysis, setDialysis] = useState<[]>();
  const [showableData, setShowableData] = useState(false);
  const [selectedRoomId, setSelectedRoomId] = useState<number>();

  useEffect(() => {
    const loadRooms = async () => {
      const responseRooms = await getRooms();
      responseRooms
        ? setRooms(responseRooms.rooms)
        : console.error('Algo salio mal al cargar los rooms');
    };
    loadRooms();
  }, []);

  useEffect(() => {
    setDialysis(undefined);
    if (!isNaN(selectedRoomId) && !isNaN(shiftId)) {
      loadDialysis();
    }
  }, [selectedRoomId, shiftId, date, shouldRefresh]);

  useEffect(() => {
    if (shiftId && date) {
      loadRoomStatus();
    }
  }, [shiftId, date, shouldRefresh]);

  const loadRoomStatus = async () => {
    const responseRoomsStatus = await getRoomStatus(shiftId, date);
    responseRoomsStatus
      ? // @ts-ignore
        setRoomStatus(responseRoomsStatus)
      : console.error('Algo salio mal al cargar los estados');
  };

  const loadDialysis = async () => {
    const responseDialysis = await getDialysisByShiftAndRoom(
      selectedRoomId,
      shiftId,
      date
    );

    if (responseDialysis) {
      setDialysis(responseDialysis[0]);
      if (responseDialysis[1]) {
        setDoctor(
          `${responseDialysis[1]?.doctorFirstName} ${responseDialysis[1]?.doctorLastName}`
        );
      }
      if (responseDialysis[2]) {
        setGraduate(
          `${responseDialysis[2]?.licenseeFirstName} ${responseDialysis[2]?.licenseeLastName}`
        );
      }
      setShowableData(true);
    } else {
      console.error('Algo salio mal al cargar las dialisis');
    }
  };

  const updateData = () => {
    loadDialysis();
    loadRoomStatus();
    refresh();
  };

  const generateRooms = () =>
    rooms?.map(room => (
      <Box
        key={room.CodSala}
        onClick={() => handleRoomSelect(room.CodSala)}
        bg={roomStatusStyle(room.CodSala)}
        sx={boxRoom}
      >
        {room.Nombre}
      </Box>
    ));

  const handleRoomSelect = roomId => {
    setDialysis(undefined);
    setSelectedRoomId(roomId);
  };

  const roomStatusStyle = currentRoomId => {
    const selected = selectedRoomId === currentRoomId;
    const roomStatusFound = roomStatus?.find(
      // @ts-ignore
      element => element.roomId === currentRoomId
    );
    // @ts-ignore
    return statusStyle(roomStatusFound?.status, selected);
  };

  const availability = (
    roomStatus?.find((x: any) => x?.roomId === selectedRoomId) as any
  )?.status;

  return (
    <DialysisContext.Provider
      value={[dialysis, { selectedRoomId, shiftId, date }]}
    >
      <VStack height='100%' sx={{ overflow: 'auto' }}>
        <Box
          sx={{
            borderRadius: 'lg',
            border: '1px dashed black',
            background: '#adbccf',
            width: '100%',
            pb: '16px',
            pt: '8px',
          }}
        >
          <Heading size='lg' sx={salasHeading}>
            Salas
          </Heading>
          {!rooms ? (
            <Skeleton isLoaded={!!rooms} sx={skeletonStyle} />
          ) : (
            <>
              <HStack
                mb='15px'
                wrap='wrap'
                justifyContent='space-evenly'
                spacing={0}
              >
                {generateRooms()}
              </HStack>

              <Divider sx={dividerStyle} />
              <Text sx={docAndGraduate}>Doctor: {doctor}</Text>

              <Text sx={docAndGraduate}>Licenciado: {graduate}</Text>
              <Divider sx={dividerStyle} />
            </>
          )}
        </Box>
        <Heading as='h2' size='lg' p='10px 0'>
          Coordinaciones de sala
        </Heading>
        {!!showableData &&
          (dialysis ? (
            <DialysisTable
              availability={availability}
              updateDialysis={updateData}
            />
          ) : (
            <Center height='70%'>
              <Spinner height='80px' width='80px' speed='0.8s' />
            </Center>
          ))}
      </VStack>
    </DialysisContext.Provider>
  );
};

export default Detail;
