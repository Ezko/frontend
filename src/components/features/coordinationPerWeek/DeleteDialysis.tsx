import React, { useContext, useState } from 'react';
import { DialysisContext } from './utils/DialysisContext';
import {
  Button,
  ButtonGroup,
  FormControl,
  FormLabel,
  Heading,
  Switch,
  Text,
} from '@chakra-ui/react';
import { heading, switchDeleteFormControl } from './utils/crudDialysisStyles';
import { deleteDialysis } from '../../../api/dialysis';

const DeleteDialysisHeader = () => {
  return <Heading sx={heading}>Eliminar cita</Heading>;
};

const DeleteDialysisBody = ({ dialysisId, onCancel, onClose }) => {
  const [replicateData, setReplicateData] = useState(false); // Utilizado para indicar si la acciona a tomar sera puntual o debe aplicar a coordinaciones futuras
  const [loading, setLoading] = useState(false);
  const [allDialysis]: any = useContext(DialysisContext);
  const dialysis = allDialysis?.find(
    element => element.dialysisId === dialysisId
  );

  const handleDelete = async () => {
    const data = {
      dialysisId: dialysisId,
      replicate: replicateData,
    };
    try {
      setLoading(!loading);
      await deleteDialysis(data);
      onClose();
    } catch (error) {
      console.error(error);
    }
  };

  const confirmationMessage = () => {
    if (dialysis) {
      const { patientFirstName, patientLastName, patientId } = dialysis;
      return (
        <>
          <Text>Desea eliminar la cita del paciente:</Text>
          <Text>
            Nombre:{' '}
            <span style={{ fontWeight: 700 }}>
              {patientFirstName} {patientLastName}
            </span>
          </Text>
          <Text>
            Documento: <span style={{ fontWeight: 700 }}>{patientId}</span>
          </Text>
        </>
      );
    } else {
      return <Text>Desea eliminar la cita del paciente seleccionado?</Text>;
    }
  };

  return (
    <>
      {confirmationMessage()}

      <FormControl sx={switchDeleteFormControl}>
        <FormLabel htmlFor='replicate' sx={{ mb: 0 }}>
          Replicar acción
        </FormLabel>
        <Switch
          id='replicate'
          size='sm'
          isChecked={replicateData}
          onChange={() => setReplicateData(!replicateData)}
        />
      </FormControl>

      <ButtonGroup isAttached w='100%' m='18px 0 10px 0'>
        <Button isFullWidth bg='gray' onClick={onCancel}>
          Cancelar
        </Button>
        <Button
          isFullWidth
          isLoading={loading}
          loadingText='Quitando'
          onClick={handleDelete}
        >
          Eliminar
        </Button>
      </ButtonGroup>
    </>
  );
};

export { DeleteDialysisBody, DeleteDialysisHeader };
