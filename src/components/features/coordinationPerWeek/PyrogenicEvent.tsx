import {
  Button,
  ButtonGroup,
  Heading,
  Textarea,
  useToast,
} from '@chakra-ui/react';
import React, { useContext, useState } from 'react';
import { updateDialysis } from '../../../api/dialysis';
import { heading } from './utils/crudDialysisStyles';
import { DialysisContext } from './utils/DialysisContext';

const PyrogenicEventHeader = () => {
  return <Heading sx={heading}>Evento pirogénico</Heading>;
};

const PyrogenicEventBody = ({
  onCancel,
  onClose,
  dialysisId,
  targetDialysis = undefined,
}) => {
  const [loading, setLoading] = useState(false);
  const [allDialysis]: any = useContext(DialysisContext);
  const dialysis =
    targetDialysis ||
    allDialysis?.find(element => element.dialysisId === dialysisId);
  const originEventDescription = dialysis?.pyrogenicEvent || '';

  const [newEventDescription, setNewEventDescription] = useState(
    originEventDescription
  );

  const toast = useToast();

  const handleSave = async e => {
    if (
      newEventDescription !== '' &&
      newEventDescription !== originEventDescription
    ) {
      const data = {
        ...dialysis,
        pyrogenicEvent: newEventDescription,
      };
      try {
        setLoading(!loading);
        await updateDialysis(data);
        onClose();
      } catch (error) {
        console.error(error);
      }
    } else {
      return toast({
        title: 'El evento no puede ser vacío o guardarse sin cambios',
        status: 'warning',
        position: 'top',
        duration: 9000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Textarea
        value={newEventDescription}
        onChange={e => setNewEventDescription(e.target.value)}
        placeholder='Descripción del evento'
        size='md'
        minHeight='150px'
      />
      <ButtonGroup isAttached w='100%' m='10px 0 10px 0'>
        <Button isFullWidth bg='gray' onClick={onCancel}>
          Cancelar
        </Button>
        <Button
          isFullWidth
          isLoading={loading}
          loadingText='Guardando'
          onClick={handleSave}
        >
          Guardar
        </Button>
      </ButtonGroup>
    </>
  );
};

export { PyrogenicEventBody, PyrogenicEventHeader };
