import { Box, Stack, Spinner, VStack } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { getShiftAndAvailability } from '../../../api/shifts';
import Detail from './Detail';
import PatientDetail from './PatientDetail';
import {
  mainCoordinationBox,
  detailWrapperBox,
  patientWrapperBox,
  shiftBox,
  shiftWaiting,
  weekAndPatientWrapper,
  weekWrapperBox,
} from './utils/coordinationStyles';
import { statusStyle } from './utils/statusStyle';
import { calculateWeek, reduceShifts } from './utils/weekUtils';
import Week from './Week';

const CoordinationPerWeek = () => {
  const [shifts, setShifts] = useState<any>(); // TODO: definir tipo
  const [selectedShift, setSelectedShift] = useState(null);
  const [week, setWeek] = useState<Date[]>(calculateWeek());
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    loadShifts();
  }, [week, refresh]);

  const loadShifts = async () => {
    const responseShifts = await getShiftAndAvailability(week[0], week[6]);
    responseShifts
      ? setShifts(reduceShifts(responseShifts))
      : console.error('Algo salio mal al cargar los turnos');
  };

  const handleRefresh = () => setRefresh(prev => !prev);

  const shiftsByDate = date => {
    const foundShifts = shifts?.find(shift => shift[`${date}`]);

    if (foundShifts) {
      foundShifts[`${date}`].sort((a, b) => a.shiftId - b.shiftId); // Ordena de menor a mayor por Id.
      return foundShifts[`${date}`]?.map((shift, index) => (
        <Box
          key={index}
          onClick={() => setSelectedShift(shift)}
          bg={shiftStatus(shift)}
          sx={shiftBox}
        >
          {shift.name[0]}
        </Box>
      ));
    }
    return <Spinner sx={shiftWaiting} speed='0.8s' />;
  };

  const shiftStatus = shift => {
    const selected = selectedShift === shift;
    return statusStyle(shift.state, selected);
  };

  return (
    <Box sx={mainCoordinationBox}>
      <Stack
        direction={{ md: 'row', sm: 'column', base: 'column' }}
        sx={{ height: '100%' }}
      >
        <Box sx={detailWrapperBox}>
          <Detail
            shiftId={selectedShift?.shiftId}
            date={selectedShift?.date}
            refresh={handleRefresh}
            shouldRefresh={refresh}
          />
        </Box>
        <VStack sx={weekAndPatientWrapper}>
          <Box sx={weekWrapperBox}>
            <Week
              shiftsByDate={shiftsByDate}
              startWeek={week}
              handleUpdateWeek={setWeek}
            />
          </Box>
          <Box sx={patientWrapperBox}>
            <PatientDetail
              shouldRefresh={refresh}
              refresh={handleRefresh}
              week={week}
            />
          </Box>
        </VStack>
      </Stack>
    </Box>
  );
};

export default CoordinationPerWeek;
