import {
  CalendarIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@chakra-ui/icons';
import {
  Box,
  Heading,
  HStack,
  IconButton,
  Tooltip,
  VStack,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import Day from './Day';
import {
  headerBox,
  headerFocusWeek,
  headerMainStack,
  headerTitle,
} from './utils/weekStyles';
import { calculateWeek, days, months, sevenDaysInMs } from './utils/weekUtils';

const Week = ({ shiftsByDate, startWeek, handleUpdateWeek }) => {
  const [week, setWeek] = useState<Date[]>(startWeek);
  const getDayName = day => days[day];

  const handleWeek = updatedWeek => {
    setWeek(updatedWeek);
    handleUpdateWeek(updatedWeek);
  };

  const getRangeDate = () =>
    `${week[0].getDate()} - ${
      months[week[0].getMonth()]
    }  al ${week[6].getDate()} - ${months[week[6].getMonth()]}`;

  const handlePreviousWeek = () => {
    handleWeek(week.map(day => new Date(day.getTime() - sevenDaysInMs)));
  };

  const handleNextWeek = () => {
    handleWeek(week.map(day => new Date(day.getTime() + sevenDaysInMs)));
  };

  return (
    <>
      <Box sx={headerBox}>
        <HStack sx={headerMainStack}>
          <IconButton
            onClick={handlePreviousWeek}
            aria-label='Semana anterior'
            variant='ghost'
            icon={<ChevronLeftIcon boxSize={8} />}
          />

          <VStack>
            <Heading size='md' sx={headerTitle}>
              Semana
            </Heading>
            <Heading as='h3' size=''>
              {getRangeDate()}
            </Heading>
          </VStack>

          <IconButton
            onClick={handleNextWeek}
            aria-label='Semana proxima'
            variant='ghost'
            icon={<ChevronRightIcon boxSize={8} />}
          />
        </HStack>

        <Tooltip label='Semana actual'>
          <IconButton
            aria-label='Focus'
            icon={<CalendarIcon />}
            variant='ghost'
            onClick={() => handleWeek(calculateWeek())}
            sx={headerFocusWeek}
          />
        </Tooltip>
      </Box>

      <HStack justifyContent='space-between'>
        {week.map((day, index) => (
          <Day key={index} title={getDayName(day.getDay())}>
            {shiftsByDate(day.getDate())}
          </Day>
        ))}
      </HStack>
    </>
  );
};

export default Week;
