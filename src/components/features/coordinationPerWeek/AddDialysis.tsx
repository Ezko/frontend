import React, { useContext, useEffect, useState } from 'react';
import { getAddDialysisData } from '../../../api/mixedData';
import { getPatientById } from '../../../api/patient';
import { DialysisContext } from './utils/DialysisContext';
import { SearchIcon } from '@chakra-ui/icons';
import {
  Button,
  ButtonGroup,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Select,
  Switch,
  useToast,
} from '@chakra-ui/react';
import {
  formControl,
  heading,
  inputDoc,
  inputNamePatient,
  switchFormControl,
} from './utils/crudDialysisStyles';
import { addDialysis } from '../../../api/dialysis';

const AddDialysisHeader = () => {
  return <Heading sx={heading}>Agregar cita</Heading>;
};

const AddDialysisBody = ({ onCancel, onClose }) => {
  const [patientId, setPatientId] = useState('');
  const [nurse, setNurse] = useState<[]>();
  const [selectedNurse, setSelectedNurse] = useState<string>();
  const [doctors, setDoctors] = useState<[]>();
  const [selectedDoctor, setSelectedDoctor] = useState<string>();
  const [graduates, setGraduates] = useState<[]>();
  const [selectedGraduate, setSelectedGraduate] = useState<string>();
  const [workStation, setWorkStation] = useState<[]>();
  const [selectedWorkStation, setSelectedWorkStation] = useState<string>();
  const [patient, setPatient] = useState<any>();
  const [replicateData, setReplicateData] = useState(false); // Utilizado para indicar si la acciona a tomar sera puntual o debe aplicar a coordinaciones futuras
  const [loading, setLoading] = useState(false);

  const toast = useToast();

  const [dialysis, { selectedRoomId, shiftId, date: targetDate }] =
    useContext(DialysisContext);

  useEffect(() => {
    const loadNurseAndworkStations = async () => {
      const resps = await getAddDialysisData(selectedRoomId);
      setNurse(resps[0].nurses);
      setWorkStation(resps[1]);
      setDoctors(resps[2].doctors);
      setGraduates(resps[3].licensees);
    };
    loadNurseAndworkStations();
  }, [dialysis]);

  useEffect(() => {
    if (patientId) {
      const getPatient = async () => {
        const resp = await getPatientById(patientId);
        setPatient(resp);
      };
      getPatient();
    } else {
      setPatient(null);
    }
  }, [patientId]);

  const generateNurseOptions = () =>
    nurse?.map((nurse: any, index) => (
      <option key={index} value={nurse.Cedula}>
        {nurse.Nombre} {nurse.Apellido}
      </option>
    ));

  const generateWorkStationOptions = () =>
    workStation?.map((wk: any, index) => (
      <option key={index} value={wk.CodMaquina}>
        {wk.CodMaquina}
      </option>
    ));

  const generateDoctorOptions = () =>
    doctors?.map((doc: any, index) => (
      <option key={index} value={doc.Cedula}>
        {doc.Nombre} {doc.Apellido}
      </option>
    ));

  const generateGraduateOptions = () =>
    graduates?.map((graduate: any, index) => (
      <option key={index} value={graduate.Cedula}>
        {graduate.Nombre} {graduate.Apellido}
      </option>
    ));

  const handleDocument = e => {
    e.preventDefault();
    const inp = e.target.value;
    if (!isNaN(inp)) setPatientId(inp);
  };

  const handleAdd = async () => {
    if (
      selectedNurse &&
      selectedDoctor &&
      selectedWorkStation &&
      selectedGraduate &&
      patient
    ) {
      const data = {
        shiftId,
        nurseId: selectedNurse,
        patientId,
        workStationId: selectedWorkStation,
        docId: selectedDoctor,
        graduateId: selectedGraduate,
        date: targetDate,
        replicate: replicateData,
      };
      try {
        setLoading(!loading);
        await addDialysis(data);
        onClose();
      } catch (error) {
        console.error(error);
      }
    } else {
      return toast({
        title: 'Debe completar todos los campos',
        status: 'warning',
        position: 'top',
        duration: 9000,
        isClosable: true,
      });
    }
  };

  return (
    <form>
      <FormControl>
        <HStack sx={{ alignItems: 'self-start' }}>
          <FormLabel>Paciente</FormLabel>
          <InputGroup size='xs'>
            <InputLeftElement>
              <SearchIcon boxSize={3} />
            </InputLeftElement>

            <Input
              variant='flushed'
              placeholder='Documento'
              value={patientId}
              onChange={handleDocument}
              sx={inputDoc}
            />
          </InputGroup>
        </HStack>

        <Input
          disabled
          variant='fileld'
          value={
            patient?.Nombre ? `${patient?.Nombre} ${patient?.Apellido}` : ''
          }
          sx={inputNamePatient}
        />
      </FormControl>

      <FormControl sx={formControl}>
        <InputGroup>
          <Select
            placeholder='Doctor'
            onChange={e => setSelectedDoctor(e.target.value)}
          >
            {!!doctors && generateDoctorOptions()}
          </Select>
          <FormLabel />
          <Select
            placeholder='Licenciado'
            onChange={e => setSelectedGraduate(e.target.value)}
          >
            {!!graduates && generateGraduateOptions()}
          </Select>
        </InputGroup>
      </FormControl>

      <FormControl sx={formControl}>
        <InputGroup>
          <Select
            placeholder='Enfermero'
            onChange={e => setSelectedNurse(e.target.value)}
          >
            {!!nurse && generateNurseOptions()}
          </Select>
          <FormLabel />
          <Select
            placeholder='Maquina'
            onChange={e => setSelectedWorkStation(e.target.value)}
          >
            {!!workStation && generateWorkStationOptions()}
          </Select>
        </InputGroup>
      </FormControl>

      <FormControl sx={switchFormControl}>
        <FormLabel htmlFor='replicate' sx={{ mb: 0 }}>
          Replicar acción
        </FormLabel>
        <Switch
          id='replicate'
          size='sm'
          isChecked={replicateData}
          onChange={() => setReplicateData(!replicateData)}
        />
      </FormControl>

      <ButtonGroup isAttached w='100%' m='10px 0 10px 0'>
        <Button isFullWidth bg='gray' onClick={onCancel}>
          Cancelar
        </Button>
        <Button
          isFullWidth
          isLoading={loading}
          loadingText='Creando'
          onClick={handleAdd}
        >
          Agregar
        </Button>
      </ButtonGroup>
    </form>
  );
};

export { AddDialysisHeader, AddDialysisBody };
