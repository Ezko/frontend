import { Box, Heading, VStack } from '@chakra-ui/layout';
import React from 'react';

const Day = ({ title, children }) => {
  return (
    <Box bg='#adbccf' borderRadius='lg' width='75px'>
      <Heading p='8px 8px 0 8px' textAlign='center' size='md'>
        {title}
      </Heading>
      <VStack pb={3}>{children}</VStack>
    </Box>
  );
};

export default Day;
