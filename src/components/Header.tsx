import { Box, Heading } from '@chakra-ui/react';
import React, { FC, ReactElement } from 'react';
import Menu from './Menu';
import { headerContainer, menu, title } from './utils/headerStyles';

interface Props {
  children: ReactElement;
}

const Header: FC<Props> = ({ children }) => {
  return (
    <>
      <Box sx={headerContainer}>
        <Heading sx={title}>Coordinacion semanal</Heading>
        <Menu
          style={menu}
        />  
        </Box>
      {children}
    </>
  );
};

export default Header;
