import { Heading } from '@chakra-ui/react';
import React from 'react';

const NotFound = () => {
  return <Heading sx={{ color: 'white' }}> Pagina no encontrada </Heading>;
};

export default NotFound;
