import { HamburgerIcon } from '@chakra-ui/icons';
import { Button, useDisclosure } from '@chakra-ui/react';
import { motion } from 'framer-motion';
import { useState } from 'react';
import { lateralCollapse } from './lateralCollapseStyles';

const useLateralCollapse = () => {
  const { isOpen } = useDisclosure();
  const [isLateralOpen, setIsLateralOpen] = useState(isOpen);
  const btn = ({ icon = null, style = {} }) => (
    <Button
      onBlur={() => setIsLateralOpen(false)}
      onClick={() => setIsLateralOpen(!isLateralOpen)}
      variant='link'
      sx={style}
    >
      {icon || <HamburgerIcon />}
    </Button>
  );
  return { btn, isLateralOpen };
};

const LatetalCollapse = ({ isLateralOpen, children }) => {
  const [hidden, setHidden] = useState(!isLateralOpen);

  return (
    <motion.div
      hidden={hidden}
      initial={false}
      onAnimationStart={() => setHidden(false)}
      onAnimationComplete={() => setHidden(!isLateralOpen)}
      animate={{ width: isLateralOpen ? 200 : 0 }}
      transition={{ duration: 0.7 }}
      style={lateralCollapse}
    >
      {children}
    </motion.div>
  );
};

export { LatetalCollapse, useLateralCollapse };
