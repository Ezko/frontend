export const sessionToken = 'Tokenggg';

export const MODAL_TARGET = {
  DELETE: 'delete',
  ADD: 'add',
  UPDATE: 'update',
  PYROGENIC: 'pyrogenic',
};

export const COMPONENT_ROUTES = {
  coordinationPerWeek: '/coordinacion-semanal',
  newPatient: '/altaPaciente',
};

export const STATUS = {
  EMPTY: 'EMPTY',
  PARTIAL: 'PARTIAL',
  FULL: 'FULL',
};
