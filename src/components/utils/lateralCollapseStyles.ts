import { MotionStyle } from "framer-motion"

export const lateralCollapse : MotionStyle = {
    background: '#1A202C',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    position: 'absolute',
    right: '0',
    height: 'calc(100vh - 56px)',
    top: '56px',
    zIndex: 999,
    boxShadow: '-5px 5px 5px black',
  };