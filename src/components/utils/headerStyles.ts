export const headerContainer = {
  position: 'absolute',
  top: 0,
  left: 0,
  height: '56px',
  width: '100%',
  textAlign: 'center',
  boxShadow: '0 5px 5px black',
  zIndex: 999,
  background: "gray.800"
};

export const title = { color: 'white', p: '8px' };

export const menuText = {
  color: 'white',
  fontSize: '16px',
  height: "100%",
  paddingTop: "8px"
};

export const menu = {
  position: 'absolute',
  right: '2%',
  top: '17px',
  color: 'white',
  fontSize: '32px',
};

export const menuItem = {
  marginTop: "15px",
  height: "40px",
  width: "200px"
};

export const closeSessionItem = {
  position: "absolute", 
  bottom: "15px", 
  right: 0,
  height: "40px",
  width: "200px"
};

export const bgHover = { background: "gray.700" }
