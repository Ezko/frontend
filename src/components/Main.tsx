import React from 'react';
import { NotAllowedIcon } from '@chakra-ui/icons';
import { Center, Heading, useMediaQuery, VStack } from '@chakra-ui/react';
import { useLocation } from 'react-router';
import { COMPONENT_ROUTES } from './utils/constants';
import Header from './Header';

const Main = ({ children }) => {
  const location = useLocation();
  const [smallScreen, largeScreen] = useMediaQuery([
    '(max-width: 1000px)',
    '(min-width: 1000px)',
  ]);

  const isPathAndRouteMatch = path => {
    for (const key in COMPONENT_ROUTES) {
      if (COMPONENT_ROUTES[key] === path) {
        return true;
      }
    }
    return false;
  };

  const getComponent = () => {
    if (smallScreen) {
      return (
        <VStack spacing='0'>
          <NotAllowedIcon boxSize='36px' sx={{ color: 'white' }} />
          <Heading sx={{ color: 'white', p: '24px' }}>
            Sitio no optimizado para smart devices.
          </Heading>
        </VStack>
      );
    }
    if (largeScreen) {
      const path = location.pathname;
      if (isPathAndRouteMatch(path)) {
        return <Header>{children}</Header>;
      } else {
        return children;
      }
    }
  };

  return (
    <Center sx={{ h: '100vh', w: '100vw', bg: 'gray.800', overflow: 'scroll' }}>
      {getComponent()}
    </Center>
  );
};
export default Main;
