import React from 'react';
import { List, ListItem, Text } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import {
  bgHover,
  closeSessionItem,
  menuItem,
  menuText,
} from './utils/headerStyles';
import { COMPONENT_ROUTES, sessionToken } from './utils/constants';
import { LatetalCollapse, useLateralCollapse } from './utils/LateralCollapse';

const Menu = ({ style }) => {
  const { btn, isLateralOpen } = useLateralCollapse();
  const handleLogOut = () => {
    sessionStorage.removeItem(sessionToken);
  };

  return (
    <>
      {btn({ style })}
      <LatetalCollapse isLateralOpen={isLateralOpen}>
        <List>
          <ListItem _hover={bgHover} sx={closeSessionItem}>
            <Link onClick={handleLogOut} to='/'>
              <Text sx={menuText}>Cerrar sesión</Text>
            </Link>
          </ListItem>
          <ListItem _hover={bgHover} sx={menuItem}>
            <Link to={COMPONENT_ROUTES.newPatient}>
              <Text sx={menuText}>Alta paciente</Text>
            </Link>
          </ListItem>
          <ListItem _hover={bgHover} sx={menuItem}>
            <Link to={COMPONENT_ROUTES.coordinationPerWeek}>
              <Text sx={menuText}>Coordinaciones</Text>
            </Link>
          </ListItem>
        </List>
      </LatetalCollapse>
    </>
  );
};

export default Menu;
