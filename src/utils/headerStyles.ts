export const headerContainer = {
  position: 'absolute',
  top: 0,
  left: 0,
  height: '56px',
  width: '100%',
  textAlign: 'center',
  boxShadow: '0 5px 5px black',
};

export const title = { color: 'white', p: '8px' };

export const closeSession = {
  color: 'white',
  position: 'absolute',
  top: '21px',
  right: '28px',
};
