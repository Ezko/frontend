import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ChakraProvider } from '@chakra-ui/react';
import Main from './components/Main';
import CoordinationPerWeek from './components/features/coordinationPerWeek';
import Login from './components/features/login';
import AuthValidator from './components/features/login/AuthValidator';
import { COMPONENT_ROUTES } from './components/utils/constants';
import NotFound from './components/NotFound';
import NewPatient from './components/features/crudPatient/newPatient';

const App = () => (
  <ChakraProvider>
    <Router>
      <Main>
        <Routes>
          <Route path='/*' element={<NotFound />} />
          <Route path='/' element={<Login />} />
          <Route
            path={COMPONENT_ROUTES.coordinationPerWeek}
            element={
              <AuthValidator>
                <CoordinationPerWeek />
              </AuthValidator>
            }
          />
          <Route
            path={COMPONENT_ROUTES.newPatient}
            element={
              <AuthValidator>
                <NewPatient />
              </AuthValidator>
            }
          />
        </Routes>
      </Main>
    </Router>
  </ChakraProvider>
);

export default App;
