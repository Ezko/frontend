export interface INewRoom {
  name: string;
  quantity: number;
}

export interface IRoom {
  Nombre: string;
  Capacidad: number;
  CodSala: number;
}
