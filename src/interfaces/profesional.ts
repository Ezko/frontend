export interface IProfesional {
  name: string;
  lastName: string;
  phone: string;
  state: boolean;
  document: number;
  email: string;
}
