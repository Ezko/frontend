export interface ITransitoryPatient {
  Tipo: string;
  Documento: string;
  Nombre: string;
  Apellido: string;
  TelPrincipal: string;
  TelSecundario: string | undefined;
  MarcadoresVirales: Array<number>;
  Correo: string;
  NumeroSeguro: string;
  FechaIngreso: string;
  FechaEgreso: string;
}

export interface IChronicPatient {
  Tipo: string;
  Documento: string;
  Nombre: string;
  Apellido: string;
  TelPrincipal: string;
  TelSecundario: string | undefined;
  MarcadoresVirales: Array<number>;
  Correo: string;
  NumeroFondo: string;
}

export interface IPatientComponent {
  name: string;
  lastName: string;
  phone: string;
  secondPhone: string;
  email: string;
  document: string;
  viralMarkers: Array<number>;
  secureNumber: string;
  leaveDate: string;
  fnrNumber: string;
}
